import logging
import os
from typing import List

import sendgrid
from sendgrid.helpers.mail import Mail, To

logger = logging.getLogger(__name__)


def send_email(sender: str, receivers: List[str], subject: str, content: str) -> bool:
    sg = sendgrid.SendGridAPIClient(api_key=os.environ.get('SENDGRID_API_KEY'))

    to_emails = [To(receiver) for receiver in receivers]  # Change to your recipient

    mail = Mail(
        from_email=sender,
        to_emails=to_emails,
        subject=subject,
        html_content=content,
        is_multiple=False,  # Avoid listing all recipients on the email
    )

    # Get a JSON-ready representation of the Mail object
    mail_json = mail.get()

    # Send an HTTP POST request to /mail/send
    response = sg.client.mail.send.post(request_body=mail_json)

    if response.status_code == 202:
        logger.info("All emails successfully sent!")
        return True
    logger.info("Error while sending emails!")
    return False
